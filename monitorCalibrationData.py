import pickle
import numpy as np


def load_pickle_file(file_name: str):
    try:
        print("Pickle.load started: " + file_name)
        with open(file_name, 'rb') as handle:
            dictionary = pickle.load(handle)
        print("Pickle.load completed: " + file_name)
        return dictionary
    except Exception as e:
        print("Cant process file: {}".format(str(file_name)), e)
        print('Fail to load pickle file: [{}]'.format(file_name))


def extend_cube(xy0, doCube=True):
    # print("deb DataFrom ", xy0)
    out_DataFrom = [xy0[0], xy0[1], xy0[2], xy0[1] * xy0[1], xy0[1] * xy0[2], xy0[2] * xy0[2]]
    if doCube:
        add_vals = [xy0[1] ** 3, xy0[1] * xy0[1] * xy0[2], xy0[1] * xy0[2] * xy0[2], xy0[2] ** 3]
        out_DataFrom += add_vals
    # print("deb out_DataFrom ", out_DataFrom)
    return out_DataFrom


def get_pred_val(DataFrom, coeffs, doCube=True):
    DataFrom = extend_cube(DataFrom, doCube=doCube)
    out_res = 0.0
    for index, cfac in enumerate(coeffs):
        out_res += (cfac * DataFrom[index])
    return out_res


class pixel2XYData:
    # ------------------------------------------------------------------------------------------------------------------
    # Load  trained model
    line_size = 5472
    cu2mm = 11.938139360299544
    ceps = np.finfo(np.float32).eps
    fov_used = 5.5 #cm 6.5, 5.5, 5.1
    print("Usage: col is width [0, 5472)")
    print("Usage: row is height [0, 3648)")

    if fov_used > 6.0:
        ref_psx0 = 0.012007565895610854   # mm
        ref_psy0 = 0.01801134884341628  # mm#0.011801134884341628   0.011897262791620454
        print("fov_used cm ", fov_used, 6.571, 4.3721)
        Xerrx = 0.01732616442096651
        Yerry = 0.018516479409990582
        print("calibration accuracy mat 9x13 errors:Xerrx, Yerry ", Xerrx, Yerry)
        solX = [-3.2555458e+01,  1.1972900e-02, -8.6847693e-05,  7.8745241e-09, 1.2157088e-08,
                1.2440978e-08, -1.3572908e-12, -2.9839279e-13, -2.4932053e-12, - 7.6216811e-13]
        solY = [2.1645172e+01, -5.3559430e-05, -1.1870146e-02,  1.4744614e-09, 5.7329999e-09,
                -4.5867637e-08, -2.4093921e-13, -1.1222913e-13, 3.5793590e-13,  7.0059514e-12]

    elif fov_used > 5.4:
            ref_psx0 = 0.010018235491819048   # mm
            ref_psy0 = 0.015027353237728571  # mm#0.011801134884341628   0.011897262791620454
            print("fov_used cm ", 5.5, 5.481978461123383, 3.647519280315565)
            Xerrx = 0.017440689456844957
            Yerry = 0.018312635978362905
            print("calibration accuracy mat 9x13 errors:Xerrx, Yerry ", Xerrx, Yerry)
            solX = [-2.6145172e+01, 9.9617094e-03, -6.3240528e-05, 2.1021265e-08, 7.8580342e-09,
                    4.0308805e-09, -2.9919435e-12, -6.8221817e-13, -1.5525359e-12, -6.1728400e-14]
            solY = [1.8567993e+01, -3.8474798e-05, -9.9592209e-03, -4.6939022e-09, 7.7125151e-10,
                    -4.8894435e-09,  6.5358829e-13,  3.0919711e-14, 3.7658765e-13,  2.2453150e-12]
    else:
        ref_psx0 = 0.009214567964536324  # mm
        ref_psy0 = 0.013821851946804485  # mm
        print("fov_used cm ", 5.1, 5.042211590194276, 3.35964741042297)
        Xerrx = 0.013433365696156586
        Yerry = 0.012887465521959735
        print("calibration accuracy mat 7x9 errors:Xerrx, Yerry ", Xerrx, Yerry)
        solX = [-2.4118826e+01, 9.1604767e-03, -6.2745530e-06, 3.2399463e-08, 5.7352225e-09,
                3.4099799e-09, -4.2022479e-12, -3.8893273e-13, -9.4951705e-13, -6.4478284e-13]
        solY = [1.6944427e+01, -5.2951917e-05, -9.2282295e-03,  7.3734441e-09, 1.7471734e-08,
                -1.4551915e-08, -9.8247799e-14, -2.8840428e-13, -2.9399261e-12,  5.8832939e-12]

    ave_ps_mm = 0.5*(ref_psx0+ref_psy0)
    use_lut = False
    pixel2x_y = dict()
    if use_lut:
        print("use_lut ", use_lut)
        if fov_used > 5.5:
            base_dr = "C:/Users/gabi/Tasks/pre_processing/monitor_machine/FOV 6.5cm/"
            pixel2xyData_file = base_dr + "FOV6.5cm_pixel2x_y_19961856_2021-06-27.pickle"
        else:
            base_dr = "C:/Users/gabi/Tasks/pre_processing/monitor_machine/FOV 6.5cm/"
            pixel2xyData_file = base_dr + "FOV5.1cm_pixel2x_y_19961856_2021-06-27.pickle"
        pixel2x_y = load_pickle_file(pixel2xyData_file)
        # total pixels = 3648*5472 =  19961856
        # len(pixel2index_x_y_shift)= 19961856
        #pixel2x_y[cpixel] = (pred_x, pred_y)
        #dictionary files loacted at: https://www.dropbox.com/home/Software%20R%26D/DB/machine_calibration/monitor


    # ----------------------------------------------------------------------------------------------------------------------

    @staticmethod
    def get_pixel_data(pixel):  # integer
        if pixel not in pixel2XYData.pixel2x_y:
            print("Error: pixel not found ", pixel)
            return ()
        else:
            return pixel2XYData.pixel2x_y[pixel]

    @staticmethod
    def get_xy_data(col, row):  # integer
        # col = x, row = y
        pixel = int(pixel2XYData.line_size * int(row + 0.5) + int(col + 0.5))
        if pixel not in pixel2XYData.pixel2x_y:
            print("Error: pixel not found ", pixel)
            return ()
        else:
            return pixel2XYData.pixel2x_y[pixel]



    @staticmethod
    def direct_pixel_data(pixel, disp=False):
        row = pixel // pixel2XYData.line_size
        col = pixel % pixel2XYData.line_size
        # DataFrom = all_dataf[index]
        DataFrom = [1.0, float(col), float(row)]
        pred_x = get_pred_val(DataFrom, pixel2XYData.solX)
        pred_y = get_pred_val(DataFrom, pixel2XYData.solY)
        if disp:
            print("mm: pred x = ", pred_x)
            print("mm: pred y = ", pred_y)
        return pred_x, pred_y


    @staticmethod
    def direct_xy_data(col, row, disp=False):
        # col = x, row = y
        # DataFrom = all_dataf[index]
        DataFrom = [1.0, float(col), float(row)]
        pred_x = get_pred_val(DataFrom, pixel2XYData.solX)
        pred_y = get_pred_val(DataFrom, pixel2XYData.solY)
        if disp:
            print("mm: pred x = ", pred_x)
            print("mm: pred y = ", pred_y)
        return pred_x, pred_y



    @staticmethod
    def get_distance_between_two_pixels_lut(pixel1, pixel2, scl=1.0):  # integer
        if pixel1 not in pixel2XYData.pixel2x_y or pixel2 not in pixel2XYData.pixel2x_y:
            print("Error: pixels not found ", pixel1, pixel2)
            return -1.0
        else:
            x1 = pixel2XYData.pixel2x_y[pixel1][1]
            x2 = pixel2XYData.pixel2x_y[pixel2][1]
            y1 = pixel2XYData.pixel2x_y[pixel1][2]
            y2 = pixel2XYData.pixel2x_y[pixel2][2]
            dx = (x2 - x1)
            dy = (y2 - y1)
            dist = np.sqrt(dx * dx + dy * dy)/scl
            row1 = pixel1 // pixel2XYData.line_size
            col1 = pixel1 % pixel2XYData.line_size
            row2 = pixel2 // pixel2XYData.line_size
            col2 = pixel2 % pixel2XYData.line_size
            dx = col2 - col1
            dy = row2 - row1
            ps_dst = np.sqrt(dx * dx + dy * dy) * pixel2XYData.ave_ps_mm/scl
            if abs(ps_dst - dist) > 3.0:
                print("get_distance_between_two_pixels_lut warn-abs(ps_dst-dist) > 3.0: choose min of dist, ps_dst ", dist, ps_dst)
                dist = np.min([ps_dst, dist])
            return dist

    @staticmethod
    def get_distance_between_two_pixels(col1, row1, col2, row2, scl=1.0):
        x1_vec, y1_vec = pixel2XYData.direct_xy_data(col1, row1)
        x1 = np.mean(x1_vec)
        y1 = np.mean(y1_vec)
        x2_vec, y2_vec = pixel2XYData.direct_xy_data(col2, row2)
        x2 = np.mean(x2_vec)
        y2 = np.mean(y2_vec)
        dx = (x2 - x1)
        dy = (y2 - y1)
        dist = np.sqrt(dx * dx + dy * dy)/scl
        dx = col2 - col1
        dy = row2 - row1
        ps_dst = np.sqrt(dx * dx + dy * dy)*pixel2XYData.ave_ps_mm/scl
        if abs(ps_dst-dist) > 3.0:
            print("get_distance_between_two_pixels warn-abs(ps_dst-dist) > 3.0: choose min of dist, ps_dst ", dist, ps_dst)
            dist = np.min([ps_dst, dist])
        return dist
